classdef IntanRHD2000 < matlab.System & matlab.system.mixin.Propagates
    % IntanRHD2000 Add summary here
    %
    % This template includes the minimum set of functions required
    % to define a System object with discrete state.

    properties(Nontunable)
        SamplingRate = rhd2000.SamplingRate.rate1000;
        numBlocksToRead = 0;
    end

    % Pre-computed constants
    properties(Access = private)
        driver;
        board;
        datablock;
        numberOfChips;
        blockSize = 60;
        NumberOfLFPChannels = 0;
        NumberOfAuxChannels = 0;
        % data = [];
        lfpData = [];
        auxData = [];
        indBoards = [];
        indLFPChannels = {};
        indAuxChannels = {};
        tfLFP = 0;
        tfAux = 0;
        tLFP;
        tAux;
        isRunning = false;
    end
    
    methods
        
        function runContinuously(obj)
            if obj.running
                return;
            end
            param = obj.board.get_configuration_parameters();
            param.Driver.NumBlocksToRead = obj.numBlocksToRead;
            obj.board.set_configuration_parameters(param);
            obj.board.run_continuously();
            obj.isRunning = true;
            %obj.setupImpl();
        end
        function val = running(obj)
            val = obj.isRunning;
        end
        function [LFP, timeLFP, Aux, timeAux, FIFOLag] = getData(obj)
            if obj.running()
                [LFP, timeLFP, Aux, timeAux, FIFOLag] = stepImpl(obj);
            elseif ~isempty(obj.auxData)
                LFP = reshape(obj.lfpData,obj.blockSize*obj.numBlocksToRead,[]);
                Aux = reshape(obj.auxData,(obj.blockSize/4)*obj.numBlocksToRead,[]);
                timeLFP = obj.tLFP+obj.tfLFP;
                timeAux = obj.tAux+obj.tfAux;
                FIFOLag = obj.board.FIFOLag;
            else
                LFP = [];
                timeLFP = [];
                Aux = [];
                timeAux = [];
                FIFOLag = [];
            end
        end
        
        function destroy(obj)
            obj.releaseImpl();
        end
        
        function init(obj)
            if ~isempty(obj.driver)
                return
            end
            obj.driver = rhd2000.Driver;
            obj.board = obj.driver.create_board();
            obj.board.SamplingRate = obj.SamplingRate;
            obj.board.run_fixed(1);
            obj.datablock = obj.board.read_next_data_block();
            for k=1:length(obj.datablock.Chips)
                if obj.board.Chips(k)
                    if isempty(obj.indLFPChannels)
                        obj.indLFPChannels{end+1} = 1:size(obj.datablock.Chips{k}.Amplifiers,1);
                        obj.indAuxChannels{end+1} = 1:size(obj.datablock.Chips{k}.AuxInputs,1);
                    else
                        obj.indLFPChannels{end+1} = obj.indLFPChannels{end}(end)+(1:size(obj.datablock.Chips{k}.Amplifiers,1));
                        obj.indAuxChannels{end+1} = obj.indAuxChannels{end}(end)+(1:size(obj.datablock.Chips{k}.AuxInputs,1));
                    end
                    obj.indBoards = [obj.indBoards k];
                end
            end
            obj.numberOfChips = length(obj.indBoards);
            for k=1:obj.numberOfChips
                obj.NumberOfLFPChannels = obj.NumberOfLFPChannels + length(obj.indLFPChannels{k});
                obj.NumberOfAuxChannels = obj.NumberOfAuxChannels + length(obj.indAuxChannels{k});
            end
            obj.lfpData = zeros(obj.blockSize,   obj.numBlocksToRead, obj.NumberOfLFPChannels);
            obj.auxData = zeros(obj.blockSize/4, obj.numBlocksToRead, obj.NumberOfAuxChannels);
            obj.tLFP = (0:obj.blockSize*obj.numBlocksToRead-1)/obj.SamplingRate.frequency;
            obj.tAux = (0:(obj.blockSize/4)*obj.numBlocksToRead-1)/obj.SamplingRate.frequency*4;
        end
    end
    
    methods(Access = protected)
        function setupImpl(obj)
            param = obj.board.get_configuration_parameters();
            param.Driver.NumBlocksToRead = obj.numBlocksToRead;
            obj.board.set_configuration_parameters(param);           
            obj.board.run_continuously();
            obj.isRunning = true;
        end

        function [LFP, timeLFP, Aux, timeAux, FIFOLag] = stepImpl(obj)
            for b=1:obj.numBlocksToRead
                obj.datablock.read_next(obj.board);
                for k=1:length(obj.indBoards)
                    obj.lfpData(:,b,obj.indLFPChannels{k}) = obj.datablock.Chips{obj.indBoards(k)}.Amplifiers';
                    obj.auxData(:,b,obj.indAuxChannels{k}) = obj.datablock.Chips{obj.indBoards(k)}.AuxInputs';
                end
            end
            LFP = reshape(obj.lfpData,obj.blockSize*obj.numBlocksToRead,[]); % obj.auxData];
            Aux = reshape(obj.auxData,(obj.blockSize/4)*obj.numBlocksToRead,[]);
            timeLFP = obj.tLFP+obj.tfLFP;
            timeAux = obj.tAux+obj.tfAux;
            FIFOLag = obj.board.FIFOLag;
            obj.tfLFP = timeLFP(end);
            obj.tfAux = timeAux(end);
        end

        function releaseImpl(obj)
            if obj.isRunning
                disp('Will disconnect the board now')
                obj.board.stop();
                obj.board.flush();
                delete(obj.datablock);
                delete(obj.board);
                disp('Connection destroyed')
            end
            obj.isRunning = false;
        end
        
        function [sz1, sz2, sz3, sz4, sz5] = getOutputSizeImpl(obj)
            if obj.NumberOfLFPChannels==0
                obj.init();
            end
            sz1 = [obj.blockSize*obj.numBlocksToRead obj.NumberOfLFPChannels];
            sz2 = [1 obj.blockSize*obj.numBlocksToRead];
            sz3 = [(obj.blockSize/4)*obj.numBlocksToRead obj.NumberOfAuxChannels];
            sz4 = [1 (obj.blockSize/4)*obj.numBlocksToRead];
            sz5 = 1;
        end
        function [fz1, fz2, fz3, fz4, fz5] = isOutputFixedSizeImpl(~)
            fz1 = true;
            fz2 = true;
            fz3 = true;
            fz4 = true;
            fz5 = true;
        end
        function [dt1, dt2, dt3, dt4, dt5] = getOutputDataTypeImpl(obj)
            dt1 = 'double';
            dt2 = 'double';
            dt3 = 'double';
            dt4 = 'double';
            dt5 = 'double';
        end
        
        function [cp1, cp2, cp3, cp4, cp5] = isOutputComplexImpl(~)
            cp1 = false;
            cp2 = false;
            cp3 = false;
            cp4 = false;
            cp5 = false;
        end
    end
    
    methods(Static)
        function [lfpSize, auxSize] = getCachedAcqParameters(SamplingRate, numBlocksToRead)
            persistent oldSamplingRate oldNumBlocksToRead oldLfpSize oldAuxSize
            if ~isempty(oldSamplingRate)
                if oldSamplingRate==SamplingRate || oldNumBlocksToRead == numBlocksToRead
                    lfpSize = oldLfpSize;
                    auxSize = oldAuxSize;
                else
                    [lfpSize, auxSize] = IntanRHD2000.getAcqParameters(SamplingRate, numBlocksToRead);
                end
            else
                [lfpSize, auxSize] = IntanRHD2000.getAcqParameters(SamplingRate, numBlocksToRead);
            end
            oldSamplingRate = SamplingRate;
            oldNumBlocksToRead = numBlocksToRead;
            oldLfpSize = lfpSize;
            oldAuxSize = auxSize;
        end
        function [lfpSize, auxSize] = getAcqParameters(SamplingRate, numBlocksToRead)
            fprintf('Querying board parameters...');
            driver = rhd2000.Driver;
            board = driver.create_board();
            board.SamplingRate = SamplingRate;
            board.run_fixed(1);
            datablock = board.read_next_data_block();
            indLFPChannels = {};
            indAuxChannels = {};
            indBoards = [];
            for k=1:length(datablock.Chips)
                if board.Chips(k)
                    if isempty(indLFPChannels)
                        indLFPChannels{end+1} = 1:size(datablock.Chips{k}.Amplifiers,1);
                        indAuxChannels{end+1} = 1:size(datablock.Chips{k}.AuxInputs,1);
                    else
                        indLFPChannels{end+1} = indLFPChannels{end}(end)+(1:size(datablock.Chips{k}.Amplifiers,1));
                        indAuxChannels{end+1} = indAuxChannels{end}(end)+(1:size(datablock.Chips{k}.AuxInputs,1));
                    end
                    indBoards = [indBoards k];
                end
            end
            numberOfChips = length(indBoards);
            NumberOfLFPChannels = 0;
            NumberOfAuxChannels = 0;
            for k=1:numberOfChips
                NumberOfLFPChannels = NumberOfLFPChannels + length(indLFPChannels{k});
                NumberOfAuxChannels = NumberOfAuxChannels + length(indAuxChannels{k});
            end
            blockSize = 60;
            lfpSize = [ blockSize   * numBlocksToRead, NumberOfLFPChannels];
            auxSize = [(blockSize/4)* numBlocksToRead, NumberOfAuxChannels]; 
            board.stop();
            board.flush();
            delete(datablock);
            delete(board);
            fprintf('done\n');
        end
    end
end
