#!/usr/bin/python

import os
import sys
import time
import uuid
import socket
import struct
import asyncore
import threading
import wave
import numpy as np
import RPi.GPIO as GPIO
from pylsl import StreamOutlet, StreamInfo

basePort = 25000
reward = {24: False, 23: False, 18: False, 15: False, 14: False}
led = {25: False, 8: False, 12: False, 16: False, 20: False, 26: False}
ir = [5, 6, 4, 17, 27, 22]

GPIO.setmode(GPIO.BCM)
GPIO.setup(reward.keys(), GPIO.OUT)
GPIO.setup(led.keys(), GPIO.OUT)
GPIO.setup(ir, GPIO.IN)


def startUpBox():

    # HL blink twice when the Pi boots up
    for k in range(2):
        GPIO.output(26, GPIO.LOW)
        time.sleep(1)
        GPIO.output(26, GPIO.HIGH)
        time.sleep(1)
    GPIO.output(26, GPIO.LOW)

    # Activate motors for 5 seconds each
    for port in reward.keys():
        drivePump(port, 1000, 5)
        time.sleep(0.25)

    # Blink LEDs
    for port in led.keys():
        GPIO.output(port, GPIO.HIGH)
        time.sleep(1)
        GPIO.output(port, GPIO.LOW)
        time.sleep(1)

    # Beep when the box is ready to roll!
    audioFile = makeAudioFeedbackFile(7600, 800, 0.5)
    os.system("aplay -f cd "+audioFile)
    time.sleep(1)
    os.system("aplay -f cd "+audioFile)
    print("Box ready!")


def drivePump(port, freq, duration):
    port = int(port)
    halfCycle = 1./freq/2.

    reward[port] = True
    tStart = time.time()
    while time.time() - tStart < duration:
        t0 = time.time()

        GPIO.output(port, GPIO.HIGH)
        while time.time() - t0 < halfCycle:
            continue

        t0 = time.time()
        GPIO.output(port, GPIO.LOW)
        while time.time() - t0 < halfCycle:
            continue
    GPIO.output(port, GPIO.LOW)
    reward[port] = False

def selectAudioFile(sel):
    if sel==1:
        return "/home/pi/beep/beep-01.wav"
    elif sel==2:
        return "/home/pi/beep/beep-02.wav"
    elif sel==3:
        return "/home/pi/beep/beep-03.wav"
    elif sel==4:
        return "/home/pi/beep/beep-04.wav"
    elif sel==5:
        return "/home/pi/beep/beep-05.wav"
    elif sel==6:
        return "/home/pi/beep/beep-06.wav"
    elif sel==7:
            return "/home/pi/beep/beep-07.wav"
    elif sel==8:
        return "/home/pi/beep/beep-08.wav"
    elif sel==9:
        return "/home/pi/beep/beep-09.wav"
    else:
        return "/home/pi/beep/beep-10.wav"

def makeAudioFeedbackFile(gain, frequency, duration):
    sampling_rate = 44100
    comptype="NONE"
    compname="not compressed"
    nchannels=1
    sampwidth=2
    num_samples = int(np.round(duration*sampling_rate))
    wav_file=wave.open("/tmp/audio.wav", 'w')
    wav_file.setparams((nchannels, sampwidth, sampling_rate, num_samples, comptype, compname))
    for x in range(num_samples):
        x_i = int(gain*np.sin(2*np.pi * frequency * x/sampling_rate))
        wav_file.writeframes(struct.pack('h', x_i))
    return "/tmp/audio.wav"


class HandlerWithMarker(object, asyncore.dispatcher_with_send):
    def __init__(self, sock=None, outlet=None, marker=None):
        asyncore.dispatcher_with_send.__init__(self, sock=sock)
        self.outlet = outlet
        if not isinstance(marker, list):
            marker = [marker]
        self.marker = marker

class HandlerLSL(object, asyncore.dispatcher_with_send):
    def __init__(self, sock=None, outlet=None):
        asyncore.dispatcher_with_send.__init__(self, sock=sock)
        self.outlet = outlet

    def handle_read(self):
        data = self.recv(1024)
        try:
            data = struct.unpack(">f", data)[0]
            if data:
                self.outlet.push_sample([data])
        except Exception as e:
            print(e)


class HandlerAudio(object, asyncore.dispatcher_with_send):
    def __init__(self, sock=None, outlet=None):
        asyncore.dispatcher_with_send.__init__(self, sock=sock)
        self.outlet = outlet

    def handle_read(self):
        data = self.recv(1024)
        try:
            sampling_rate = 44100
            gain, frequency, duration, marker = struct.unpack(">4d", data)
            # print((gain, frequency, duration, marker))
            audioFile = selectAudioFile(frequency)
            # audioFile = makeAudioFeedbackFile(gain, frequency, duration)
            self.outlet.push_sample([marker])
            os.system("aplay -f cd "+audioFile+" &")

        except Exception as e:
            print(e)


class HandlerIR(HandlerWithMarker):
    def handle_read(self):
        data = self.recv(1024)
        try:
            data = struct.unpack(">?", data)[0]
            if data:
                self.outlet.push_sample(self.marker)
        except:
            pass


class HandlerLED(HandlerWithMarker):
    def handle_read(self):
        data = self.recv(1024)
        try:
            data = struct.unpack(">?", data)[0]
            if data:
                self.outlet.push_sample(self.marker)
        except:
            pass


class HandlerReward(HandlerWithMarker):
    def handle_read(self):
        data = self.recv(1024)
        try:
            port, duration, freq = struct.unpack(">3d", data)
            port = int(port)
            print(port, duration, freq)
            if reward[port]:
                print('Port '+str(port)+' is in use')
                return
            self.outlet.push_sample(self.marker)
            worker = threading.Thread(target=drivePump, args=(port, freq, duration))
            worker.daemon = True
            worker.start()
            worker.join()
        except:
            pass


class Server(asyncore.dispatcher):

    def __init__(self, host, port, handlerType="reward", outlet=None, marker=None):
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind((host, port))
        self.listen(5)
        self.handlerType = handlerType
        self.outlet = outlet
        self.marker = [marker]

    def handle_accept(self):
        pair = self.accept()
        if pair is not None:
            sock, addr = pair
            print 'Incoming connection from %s' % repr(addr)
            if self.handlerType == "reward":
                handler = HandlerReward(sock=sock, outlet=self.outlet, marker=self.marker)
            elif self.handlerType == "audio":
                handler = HandlerAudio(sock=sock, outlet=self.outlet)
            elif self.handlerType == "led":
                handler = HandlerLED(sock=sock, outlet=self.outlet, marker=self.marker)
            elif self.handlerType == "ir":
                handler = HandlerIR(sock=sock, outlet=self.outlet, marker=self.marker)
            else:
                handler = HandlerLSL(sock=sock, outlet=self.outlet)


def main(argv=None):
    if argv is None:
        argv = sys.argv

    # Run box start up sequence
    startUpBox()

    # Create LSL outlet
    info = StreamInfo(name="BrainER_" + socket.gethostname(),
                      type="Markers",
                      channel_format='float32',
                      channel_count=1,
                      source_id=str(uuid.uuid4()))
    outlet = StreamOutlet(info)

    try:
        Reward1 = Server('localhost', basePort+1, "reward", outlet=outlet, marker=1)
        Reward2 = Server('localhost', basePort+2, "reward", outlet=outlet, marker=2)
        Reward3 = Server('localhost', basePort+3, "reward", outlet=outlet, marker=3)
        Reward4 = Server('localhost', basePort+4, "reward", outlet=outlet, marker=4)
        Reward5 = Server('localhost', basePort+5, "reward", outlet=outlet, marker=5)
        Reward6 = Server('localhost', basePort+6, "reward", outlet=outlet, marker=6)

        AudioFeedback = Server('localhost', basePort+7, "audio", outlet=outlet)

        LED1 = Server('localhost', basePort+10+1, "led", outlet=outlet, marker=10+1)
        LED2 = Server('localhost', basePort+10+2, "led", outlet=outlet, marker=10+2)
        LED3 = Server('localhost', basePort+10+3, "led", outlet=outlet, marker=10+3)
        LED4 = Server('localhost', basePort+10+4, "led", outlet=outlet, marker=10+4)
        LED5 = Server('localhost', basePort+10+5, "led", outlet=outlet, marker=10+5)
        LED6 = Server('localhost', basePort+10+6, "led", outlet=outlet, marker=10+6)
        HL   = Server('localhost', basePort+10+7, "led", outlet=outlet, marker=10+7)

        IR1  = Server('localhost', basePort+100+1, "ir", outlet=outlet, marker=100+1)
        IR2  = Server('localhost', basePort+100+2, "ir", outlet=outlet, marker=100+2)
        IR3  = Server('localhost', basePort+100+3, "ir", outlet=outlet, marker=100+3)
        IR4  = Server('localhost', basePort+100+4, "ir", outlet=outlet, marker=100+4)
        IR5  = Server('localhost', basePort+100+5, "ir", outlet=outlet, marker=100+5)

        GenericLSL = Server('localhost', basePort + 1000 + 1, "lsl", outlet=outlet)

        asyncore.loop()
        
    except Exception as e:
        print(e)

    finally:
        GPIO.cleanup()
        print('Good bye!')


if __name__ == "__main__":
    sys.exit(main())
