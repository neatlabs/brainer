# /bin/sh

# Install RPI kernel headers
git clone https://github.com/aojeda/rpi-source.git
cd rpi-source
chmod +x rpi-source
./rpi-source --skip-gcc
cd ..


# Compile the rtl8822bu wifi driver
git clone https://github.com/aojeda/rtl8822bu.git
cd rtl8822bu
make
make install
cd ..

# Update RPi system
apt-get update
apt-get -y upgrade
apt-get -y dist-upgrade
apt-get -y install libncurses5-dev bc build-essential git
pip install pylsl
mv liblsl-bcm2708.so /usr/local/lib/python2.7/dist-packages/pylsl/liblsl32.so

# Cleanup
rm -r rpi-source/
rm -r rtl8822bu/
rm -r linux-*


# Configure network interfaces
echo ""
echo "Configure network interfaces"
rm /etc/network/interfaces
echo "auto lo" >> /etc/network/interfaces
echo "iface lo inet loopback" >> /etc/network/interfaces
echo "" >> /etc/network/interfaces

echo "auto eth0" >> /etc/network/interfaces
echo "iface eth0 inet dhcp" >> /etc/network/interfaces
echo "" >> /etc/network/interfaces

echo "auto wlan0" >> /etc/network/interfaces
echo "allow-hotplug wlan0" >> /etc/network/interfaces
echo "iface wlan0 inet static" >> /etc/network/interfaces
echo "address "$1 >> /etc/network/interfaces
echo "netmask 255.255.255.0" >> /etc/network/interfaces
echo "gateway 192.168.0.1" >> /etc/network/interfaces
echo "wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf" >> /etc/network/interfaces
echo "iface default inet dhcp" >> /etc/network/interfaces
echo "" >> /etc/network/interfaces

echo "allow-hotplug wlan1" >> /etc/network/interfaces
echo "iface wlan1 inet manual" >> /etc/network/interfaces
echo "wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf" >> /etc/network/interfaces

rm /etc/wpa_supplicant/wpa_supplicant.conf
echo "network={" >> /etc/wpa_supplicant/wpa_supplicant.conf
echo ssid='"'$2'"' >> /etc/wpa_supplicant/wpa_supplicant.conf
echo psk='"'$3'"' >> /etc/wpa_supplicant/wpa_supplicant.conf
echo "proto=RSN" >> /etc/wpa_supplicant/wpa_supplicant.conf
echo "key_mgmt=WPA-PSK" >> /etc/wpa_supplicant/wpa_supplicant.conf
echo "pairwise=CCMP" >> /etc/wpa_supplicant/wpa_supplicant.conf
echo "auth_alg=OPEN" >> /etc/wpa_supplicant/wpa_supplicant.conf
echo "}" >> /etc/wpa_supplicant/wpa_supplicant.conf
echo "" >> /etc/wpa_supplicant/wpa_supplicant.conf
echo "done"

# Configure SimBSI
chmod +x rc.local
chmod +x simbsi_rpi.py
mv rc.local /etc/rc.local
mv cmdline.txt /boot/cmdline.txt

reboot
