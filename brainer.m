function fig = brainer()
fig = findall(0,'Tag','runOnTargetHardware');
if ~isempty(fig)
    return;
end

devFile = fullfile(fileparts(which('brainer')),'resources','rpiDevices.mat');
if ~exist(devFile,'file'), scan;end
if exist(devFile,'file')
    load(devFile); %#ok
else    
    dev = {};
end

n = length(dev);
state = cell(n,4);
rmthis = [];
for k=1:n
    try
        p = raspberrypi(dev{k}, 'pi','raspberry');
        [runningState,runningTask] = getRunningTask(p);
        state{k,1} = false;
        state{k,2} = dev{k};
        state{k,3} = runningState;
        state{k,4} = runningTask;
    catch
        rmthis(end+1) = k; %#ok
    end
end
state(rmthis,:) = [];
ColumnWidth = {50 100 100 225};
ColumnName = {'Select','IP Address','State','Task'};
fig = figure('ToolBar','none','MenuBar','none','NumberTitle','off','Name','BrainER','Visible','On','Tag','runOnTargetHardware');
fig.Position(3:4) = [509   262];
verticalLayout = uix.VBoxFlex( 'Parent', fig, 'Spacing', 2, 'DividerMarkings','off');
tab = uitable(verticalLayout,'Position',[4 5 554 250],'Units','normalized','Tag','runOnTargetHardwareTable',...
    'ColumnEditable',true,...
    'ColumnWidth',ColumnWidth,...
    'Data',state);
tab.Position = [0.0043 0.1351 0.9957 0.8456];
tab.ColumnName = ColumnName;
bottomPanelHLayout = uix.HBoxFlex( 'Parent', verticalLayout, 'Spacing', 2, 'DividerMarkings','off','Spacing',0.1);
verticalLayout.Heights = [-0.8 -0.2];
iconSize = [45 45];
rootDir = fileparts(which('brainer'));
iconLib = imresize(imread(fullfile(rootDir,'icons','Gnome-applications-office.svg.png')),iconSize);
iconNew = imresize(imread(fullfile(rootDir,'icons','new-brainer.png')),iconSize);
iconOpen = imresize(imread(fullfile(rootDir,'icons','Gnome-document-open.svg.png')),iconSize);
iconRun = imresize(imread(fullfile(rootDir,'icons','run-brainer.png')),iconSize);
iconStop = imresize(imread(fullfile(rootDir,'icons','Gnome-media-playback-stop.svg.png')),iconSize);
iconClean = imresize(imread(fullfile(rootDir,'icons','Gnome-edit-clear.svg.png')),iconSize);
iconScan = imresize(imread(fullfile(rootDir,'icons','Gnome-view-refresh.svg.png')),iconSize);
iconReboot = imresize(imread(fullfile(rootDir,'icons','Gnome-system-shutdown.svg.png')),iconSize);
iconUpdate = imresize(imread(fullfile(rootDir,'icons','Gnome-system-software-update.svg.png')),iconSize);
iconHelp = imresize(imread(fullfile(rootDir,'icons','Gnome-help-browser.svg.png')),iconSize);

uicontrol(bottomPanelHLayout,'Style', 'pushbutton', 'CData',iconLib,'TooltipString','Simulink library browser','Callback','slLibraryBrowser');
uicontrol(bottomPanelHLayout,'Style', 'pushbutton', 'CData',iconNew,'TooltipString','New task','Callback',@newTask);
uicontrol(bottomPanelHLayout,'Style', 'pushbutton', 'CData',iconOpen,'TooltipString','Open task','Callback',@openTask,'BackgroundColor',[1 1 1]);
uicontrol(bottomPanelHLayout,'Style', 'pushbutton', 'CData',iconRun, 'TooltipString','Run', 'Callback',@runTask,'BackgroundColor',[1 1 1]);
uicontrol(bottomPanelHLayout,'Style', 'pushbutton', 'CData',iconStop,'TooltipString','Stop','Callback',@stopTask,'BackgroundColor',[1 1 1]);
uicontrol(bottomPanelHLayout,'Style', 'pushbutton', 'CData',iconClean,'TooltipString','Clean pre-compiled task','Callback',@cleanTask,'BackgroundColor',[1 1 1]);
uicontrol(bottomPanelHLayout,'Style', 'pushbutton', 'CData',iconScan,'TooltipString','Find new devices','Callback',@scan,'BackgroundColor',[1 1 1]);
uicontrol(bottomPanelHLayout,'Style', 'pushbutton', 'CData',iconReboot,'TooltipString','Reboot device','Callback',@rebootDevice,'BackgroundColor',[1 1 1]);
uicontrol(bottomPanelHLayout,'Style', 'pushbutton', 'CData',iconUpdate,'TooltipString','Update device configuration','Callback',@updateDevice,'BackgroundColor',[1 1 1]);
uicontrol(bottomPanelHLayout,'Style', 'pushbutton', 'CData',iconHelp,'TooltipString','Help','Callback','web(''https://bitbucket.org/neatlabs/brainer/wiki/Home'',''-browser'')','BackgroundColor',[1 1 1]);
end


function [runningState,runningTask] = getRunningTask(p)
fileList = listSimulinkTasks(p);
runningState = 'Idle';
runningTask = '';
for k=1:length(fileList)
    tmp = p.system(['ps aux | grep ' fileList{k}]);
    if length(strfind(tmp,sprintf('\n')))>2 %#ok
        runningTask = fileList{k};
        runningState = 'Running';
        break
    end
end
end


function newTask(src,evnt)
in = inputdlg({'Pipeline name', 'Step time (sec)'},'',1,{'',''});
if isempty(in)
    return;
end
sys = new_system(in{1});
cs = getActiveConfigSet(sys);
cs.set_param('StartTime','0');
cs.set_param('StopTime','inf');
cs.set_param('SolverType','fixed-step');
cs.set_param('FixedStep',in{2});
cs.set_param('DSMLogging','off');
cs.set_param('SignalLogging','off');
cs.set_param('SaveOutput','off');
cs.set_param('SaveState','off');
cs.set_param('SaveTime','off');
open_system(sys)
end


function openTask(src,evnt)
persistent PathName
if isempty(PathName), PathName = pwd;end
[FileName,PathName,FilterIndex] = uigetfile({'*.slx' 'Simulink (.slx)'},'Select file', PathName);
if FilterIndex~=0
    filename = fullfile(PathName,FileName);
    fig = findall(0,'Tag','runOnTargetHardware');
    set(fig,'pointer','watch');
    set(findobj(fig,'Enable','On'),'Enable','Off')
    drawnow;
    open(filename);
    set(findobj(fig,'Enable','Off'),'Enable','On')
    set(fig,'pointer','arrow');
end
end


function scan(src,evnt)
dev = {};
devName = {};
fig = findall(0,'Tag','runOnTargetHardware');
set(fig,'pointer','watch');
set(findobj(fig,'Enable','On'),'Enable','Off')
drawnow;
disp('Scanning...')
for k=90:103
    try
        p = raspberrypi(['192.168.0.' num2str(k)], 'pi','raspberry');
        dev{end+1} = p.DeviceAddress;
        devName{end+1} = deblank(p.system('cat /etc/hostname'));
        disp(p.DeviceAddress);
    end
end
disp('done')
[~,loc] = unique(devName);
dev = dev(loc);
saveIn = fullfile(fileparts(which('brainer')),'resources','rpiDevices.mat');
save(saveIn,'dev');

tab = findall(0,'Tag','runOnTargetHardwareTable');
oldState = tab.Data;
n = length(dev);
newState = cell(n,4);
for k=1:n
    newState{k,1} = false;
    newState{k,2} = dev{k};
    if any(ismember(oldState(:,2),dev{k}))
        newState{k,3} = oldState{ismember(oldState(:,2),dev{k}),3};
        newState{k,4} = oldState{ismember(oldState(:,2),dev{k}),4};
    else
        newState{k,3} = 'Idle';
        newState{k,4} = '';
    end
end
tab.Data = newState;
set(findobj(fig,'Enable','Off'),'Enable','On')
set(fig,'pointer','arrow');
end


function fileList = listSimulinkTasks(p)
files = p.system('ls *.elf');
ind = strfind(files,'.elf');
if isempty(ind)
    msgbox(['Device ' dev{k} ' does not have pre-built task available. You need to compile the desired task on the selected hardware first.'], 'Error','error');
end
ptr = 1;
fileList = cell(length(ind),1);
for i=1:length(ind)
    fileList{i} = files(ptr:ind(i)-1);
    ptr = ind(i)+5;
end
end


function runTask(src,evnt)
tab = findall(0,'Tag','runOnTargetHardwareTable');
state = tab.Data;
target = find(cell2mat(state(:,1)));
if isempty(target)
    return;
end
fig = findall(0,'Tag','runOnTargetHardware');
set(fig,'pointer','watch');
set(findobj(fig,'Enable','On'),'Enable','Off')
drawnow;

dev = state(target,2);
for k=1:length(target)
    
    % Connect to a RPi
    p = raspberrypi(dev{k}, 'pi','raspberry');
    if strcmp(state{target(k),3},'Running')
        msgbox(['Device ' dev{k} ' is busy. You need to stop the task running on it first.'], 'Error','error');
        continue;
    end
    
    % List models available in the selected device
    fileList = listSimulinkTasks(p);
      
    % Select task to run
    sel = listdlg('liststring',fileList,'SelectionMode','single','PromptString',dev{k},'OKString','Select');
    if isempty(sel)
        set(findobj(fig,'Enable','Off'),'Enable','On')
        set(fig,'pointer','arrow');
        return
    end
    
    % Run the task
    runModel(p,fileList{sel});
    state{target(k),4} = fileList{sel};
    for i=1:10
        if isModelRunning(p,fileList{sel})
            break
        end
        pause(0.5);
    end
    if ~isModelRunning(p,fileList{sel})
        state{target(k),3} = 'Fail';
    else
        state{target(k),3} = 'Running';
    end
end
tab.Data = state;
set(findobj(fig,'Enable','Off'),'Enable','On')
set(fig,'pointer','arrow');
end


function stopTask(src,evnt)
tab = findall(0,'Tag','runOnTargetHardwareTable');
state = tab.Data;
target = find(cell2mat(state(:,1)));
if isempty(target)
    return;
end
fig = findall(0,'Tag','runOnTargetHardware');
set(fig,'pointer','watch');
set(findobj(fig,'Enable','On'),'Enable','Off')
drawnow;
dev = state(target,2);
taskName = state(target,4);
for k=1:length(target)
    
    % Connect to a RPi
    p = raspberrypi(dev{k}, 'pi','raspberry');
    
    % Stop task
    if(isModelRunning(p,taskName{k}))        
        stopModel(p, taskName{k});
        while isModelRunning(p,taskName{k})
            pause(0.5);
        end
        state{target(k),3} = 'Idle';
        state{target(k),4} = '';
    end
end
tab.Data = state;
set(findobj(fig,'Enable','Off'),'Enable','On')
set(fig,'pointer','arrow');
end


function cleanTask(src,evnt)
tab = findall(0,'Tag','runOnTargetHardwareTable');
state = tab.Data;
target = find(cell2mat(state(:,1)));
if isempty(target)
    return;
end
fig = findall(0,'Tag','runOnTargetHardware');
set(fig,'pointer','watch');
set(findobj(fig,'Enable','On'),'Enable','Off')
drawnow;
dev = state(target,2);
for k=1:length(target)
    
    % Connect to a RPi
    p = raspberrypi(dev{k}, 'pi','raspberry');
    
    % List models available in the selected device
    fileList = listSimulinkTasks(p);
      
    % Select task to run
    sel = listdlg('liststring',fileList,'SelectionMode','single','PromptString',dev{k},'OKString','Select');
    if isempty(sel)
        set(findobj(fig,'Enable','Off'),'Enable','On')
        set(fig,'pointer','arrow');
        return
    end
    
    % Clean
    p.system(['rm -rf ' fileList{sel} '*'])
end

set(findobj(fig,'Enable','Off'),'Enable','On')
set(fig,'pointer','arrow');
end


function updateDevice(src,evnt)
tab = findall(0,'Tag','runOnTargetHardwareTable');
state = tab.Data;
target = find(cell2mat(state(:,1)));
if isempty(target)
    return;
end
dev = state(target,2);
rootDir = fileparts(which('brainer'));
for k=1:length(target)
    
    % Connect to a RPi
    p = raspberrypi(dev{k}, 'pi','raspberry');
    
    % Update scripts
    p.putFile(fullfile(rootDir,'rc.local'))
    p.system('chmod +x rc.local');
    p.putFile(fullfile(rootDir,'simbsi_rpi.py'));
    p.system('chmod +x simbsi_rpi.py');
    p.system('sudo mv rc.local /etc/rc.local');
    p.putFile(fullfile(rootDir,'cmdline.txt'));
    p.system('sudo mv cmdline.txt /boot/cmdline.txt');
end

% Trigger a reboot
disp('Triggering a reboot!');
rebootDevice(src,evnt);
end


function rebootDevice(src,evnt)
tab = findall(0,'Tag','runOnTargetHardwareTable');
state = tab.Data;
target = find(cell2mat(state(:,1)));
if isempty(target)
    return;
end
fig = findall(0,'Tag','runOnTargetHardware');
set(fig,'pointer','watch');
set(findobj(fig,'Enable','On'),'Enable','Off')
drawnow;
dev = state(target,2);
for k=1:length(target)
    
    % Connect to a RPi
    p = raspberrypi(dev{k}, 'pi','raspberry');
    p.system('sudo shutdown -r +1');
end
pause(60);

% Waint until Pi is back online
for k=1:length(target)
    p = [];
    while isempty(p)
        try %#ok
            % Connect to a RPi
            p = raspberrypi(dev{k}, 'pi','raspberry');
        end
    end
end
set(findobj(fig,'Enable','Off'),'Enable','On')
set(fig,'pointer','arrow');
end